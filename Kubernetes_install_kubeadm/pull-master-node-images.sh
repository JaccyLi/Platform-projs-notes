#!/usr/bin/env bash

IMAGES=(kube-apiserver:v1.23.4
kube-controller-manager:v1.23.4
kube-scheduler:v1.23.4
kube-proxy:v1.23.4
pause:3.6
etcd:3.5.1-0
coredns:v1.8.6)

for img in ${IMAGES[@]}
do
    docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$img
done
