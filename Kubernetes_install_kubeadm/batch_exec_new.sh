#!/usr/bin/env bash

#HOST=(192.168.32.240 192.168.32.241 192.168.32.242) 
HOST=(node01.suosuoli.cn node02.suosuoli.cn node03.suosuoli.cn) 

if [[ $# -eq 0 ]]
then
    printf "Usage: %s: [-c <cmd>] [-t <file> <path>]\n" $0
fi

cflag=
tflag=
while getopts c:t: name
do
    case $name in
    c)    cflag=1
          cval="$OPTARG";;
    t)    tflag=1
          tval="$OPTARG";;
    *)   printf "Usage: %s: [-c <cmd>] [-t <file> <path>]\n" $0
          exit 2;;
    esac
done

exec_cmd() {
    CMD=$1
    if [[ -z $CMD ]]
    then
        echo "Usage: $(basename $0) -c <cmd>"
        exit
    fi
    
    for h in ${HOST[@]}
    do
        echo "EXEC: ssh -A root@$h $CMD"
        echo "-------------"
        ssh -A root@$h $CMD
        echo
    done
}

transferFile() {
    if [[ -z $2 ]]
    then
        echo "Usage: $(basename $0) -t <src file> <dest path>"
        exit
    fi
    for h in ${HOST[@]}
    do
        echo "scp $1 root@$h:$2"
        echo "-------------"
        scp $1 root@$h:$2
	echo
    done
}

if [ ! -z "$cflag" ]; then
    exec_cmd "${cval[@]}"
fi
if [ ! -z "$tflag" ]; then
    transferFile ${tval[0]} ${tval[1]}
fi
