install Kubernetes
===

- [ubuntu2004](http://mirrors.ustc.edu.cn/ubuntu-releases/20.04.3/ubuntu-20.04.3-live-server-amd64.iso)

cluster ips
---

- small cluster
```ini
192.168.32.240	node01.suosuoli.cn
192.168.32.241	node02.suosuoli.cn
192.168.32.242	node03.suosuoli.cn
```

```ini
IP	主机名	角色
192.168.32.200	control01.suosuoli.cn	K8s 集群主节点 1
192.168.32.201	control02.suosuoli.cn	K8s 集群主节点 2
192.168.32.202	control03.suosuoli.cn	K8s 集群主节点 3

192.168.32.210	node01.suosuoli.cn	K8s 集群工作节点 1
192.168.32.211	node02.suosuoli.cn	K8s 集群工作节点 2
192.168.32.212	node03.suosuoli.cn	K8s 集群工作节点 3

192.168.32.220	etcd.suosuoli.cn	集群状态存储 etcd

192.168.32.221	ha1.suosuoli.cn	K8s 主节点访问入口 1(高可用及负载均衡)
192.168.32.222	ha2.suosuoli.cn	K8s 主节点访问入口 1(高可用及负载均衡)
192.168.32.230	harbor.suosuoli.cn	容器镜像仓库
192.168.32.100  无	VIP
```

steps
---

- selinux,swapoff

```bash
# selinux
apt install policycoreutils
## check if selinux is closed default
sestatus
root@node01:~# sestatus
SELinux status:                 disabled
## if not disabled, then
[[ -f /etc/selinux/config]] && sed -i 's/enforcing/disabled/' /etc/selinux/config
setenforce 0

# swap off
swapoff -a
## make sure dont mount on boot
sed -ri.bak 's/.*swap.*/#&/' /etc/fstab
```

- kubelet kubeadm kubectl

```bash
#config
sudo modprobe br_netfilter
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

#install
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
apt-get update && apt-get install -y apt-transport-https
curl https://mirrors.aliyun.com/kubernetes/apt/doc/apt-key.gpg | apt-key add - 
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://mirrors.aliyun.com/kubernetes/apt/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl

# kubeadm completion
mkdir ~/.kube/
kubeadm completion bash >> ~/.kube/kubeadm-bash-completion
echo "source ~/.kube/kubeadm-bash-completion" >> ~/.bash_profile
# kubectl completion
kubectl completion bash >> ~/.kube/kubectl-bash-completion
echo "source ~/.kube/kubectl-bash-completion" >> ~/.bash_profile

# make kubelet and docker use systemd cgroups
vim /usr/lib/systemd/system/docker.service
ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock --exec-opt native.cgroupdriver=systemd
```

- docker

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo docker run hello-world
```

- pull all images blow to all nodes
```bash
#!/usr/bin/env bash

IMAGES=(kube-apiserver:v1.23.4
kube-controller-manager:v1.23.4
kube-scheduler:v1.23.4
kube-proxy:v1.23.4
pause:3.6
etcd:3.5.1-0
coredns:v1.8.6)

for img in ${IMAGES[@]}
do
    docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$img
done
```

- retag coredns
```bash
# kubeam default cordns image from 'k8s.gcr.io/coredns/coredns:v1.8.6'
# but from aliyunce, the image location is 'registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:v1.8.6', the folder is different, so we should tag a now image 
# to '.../coredns/coredns:v1.8.6' format
docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:v1.8.6 registry.cn-hangzhou.aliyuncs.com/google_containers/coredns/coredns:v1.8.6
```

- kubeadm init
```bash
# 1 run on node1(240)
kubeadm init --apiserver-advertise-address="192.168.32.240" \
    --image-repository="registry.cn-hangzhou.aliyuncs.com/google_containers" \
    --kubernetes-version="v1.23.4" \
    --service-cidr="10.96.200.0/24" \
    --pod-network-cidr="172.16.0.0/16"
#--------log-----------------
root@node01:~# kubeadm init --apiserver-advertise-address="192.168.32.240" \
registr>     --image-repository="registry.cn-hangzhou.aliyuncs.com/google_containers" \
es-versi>     --kubernetes-version="v1.23.4" \
>     --service-cidr="10.96.200.0/24" \
>     --pod-network-cidr="172.16.0.0/16"
[init] Using Kubernetes version: v1.23.4
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local node01.suosuoli.cn] and IPs [10.96.200.1 192.168.32.240]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [localhost node01.suosuoli.cn] and IPs [192.168.32.240 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [localhost node01.suosuoli.cn] and IPs [192.168.32.240 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 11.006810 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.23" in namespace kube-system with the configuration for the kubelets in the cluster
NOTE: The "kubelet-config-1.23" naming of the kubelet ConfigMap is deprecated. Once the UnversionedKubeletConfigMap feature gate graduates to Beta the default name will become just "kubelet-config". Kubeadm upgrade will handle this transition transparently.
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node node01.suosuoli.cn as control-plane by adding the labels: [node-role.kubernetes.io/master(deprecated) node-role.kubernetes.io/control-plane node.kubernetes.io/exclude-from-external-load-balancers]
[mark-control-plane] Marking the node node01.suosuoli.cn as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: tfjte6.14c9hkudv9icwiuk
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.32.240:6443 --token tfjte6.14c9hkudv9icwiuk \
        --discovery-token-ca-cert-hash sha256:8ae6acb9d1223466fb0d1cf3088a0cc0116a4cf00ebb34d5cf26e54a7e30793c
#--------log-----------------

# 2 put config and export admin.conf
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

export KUBECONFIG=/etc/kubernetes/admin.conf
echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> /etc/profile.d/k8s-admin.conf

# 3 install pod network
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
#--------log-----------------
root@node01:~# kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
configmap/calico-config created
customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/caliconodestatuses.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/ipreservations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/kubecontrollersconfigurations.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org created
customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org created
clusterrole.rbac.authorization.k8s.io/calico-kube-controllers created
clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers created
clusterrole.rbac.authorization.k8s.io/calico-node created
clusterrolebinding.rbac.authorization.k8s.io/calico-node created
daemonset.apps/calico-node created
serviceaccount/calico-node created
deployment.apps/calico-kube-controllers created
serviceaccount/calico-kube-controllers created
Warning: policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
poddisruptionbudget.policy/calico-kube-controllers created
#--------log-----------------

# join node02(run on 241)
kubeadm join 192.168.32.240:6443 --token tfjte6.14c9hkudv9icwiuk \
        --discovery-token-ca-cert-hash sha256:8ae6acb9d1223466fb0d1cf3088a0cc0116a4cf00ebb34d5cf26e54a7e30793c
#--------log-----------------
root@node02:~# kubeadm join 192.168.32.240:6443 --token tfjte6.14c9hkudv9icwiuk \
>         --discovery-token-ca-cert-hash sha256:8ae6acb9d1223466fb0d1cf3088a0cc0116a4cf00ebb34d5cf26e54a7e30793c
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
W0220 08:22:07.682431   27609 utils.go:69] The recommended value for "resolvConf" in "KubeletConfiguration" is: /run/systemd/resolve/resolv.conf; the provided value is: /run/systemd/resolve/resolv.conf
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
#--------log-----------------

# join command will expire after 2 hours
# join node03(run on 242)
kubeadm join 192.168.32.240:6443 --token tfjte6.14c9hkudv9icwiuk \
        --discovery-token-ca-cert-hash sha256:8ae6acb9d1223466fb0d1cf3088a0cc0116a4cf00ebb34d5cf26e54a7e30793c
#--------log-----------------
root@node03:~# kubeadm join 192.168.32.240:6443 --token tfjte6.14c9hkudv9icwiuk \
>         --discovery-token-ca-cert-hash sha256:8ae6acb9d1223466fb0d1cf3088a0cc0116a4cf00ebb34d5cf26e54a7e30793c
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster...
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
W0220 08:22:26.076578   27188 utils.go:69] The recommended value for "resolvConf" in "KubeletConfiguration" is: /run/systemd/resolve/resolv.conf; the provided value is: /run/systemd/resolve/resolv.conf
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

This node has joined the cluster:
* Certificate signing request was sent to apiserver and a response was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the control-plane to see this node join the cluster.
#--------log-----------------

# print a new join command
kubeadm token create --print-join-command

# label worker node
kubectl label nodes node02.suosuoli.cn node-role.kubernetes.io/worker='02'

# modify network to lvs mode, instead of iptables
root@node01:~# kubectl edit cm kube-proxy -n kube-system
----
33     ipvs:
 34       excludeCIDRs: null
 35       minSyncPeriod: 0s
 36       scheduler: ""
 37       strictARP: false
 38       syncPeriod: 0s
 39       tcpFinTimeout: 0s
 40       tcpTimeout: 0s
 41       udpTimeout: 0s
 42     kind: KubeProxyConfiguration
 43     metricsBindAddress: ""
 44     mode: "ipvs" ####modify here
----
configmap/kube-proxy edited
# delete kube-proxy, then it will auto start a new one using new config
root@node01:~# kubectl get pods -A -o wide
NAMESPACE     NAME                                         READY   STATUS    RESTARTS   AGE   IP               NODE                 NOMINATED NODE   READINESS GATES
kube-system   calico-kube-controllers-566dc76669-hmj7c     1/1     Running   0          24m   172.16.121.130   node01.suosuoli.cn   <none>           <none>
kube-system   calico-node-cm67k                            1/1     Running   0          20m   192.168.32.241   node02.suosuoli.cn   <none>           <none>
kube-system   calico-node-jlhc9                            1/1     Running   0          24m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
kube-system   calico-node-zlhll                            1/1     Running   0          20m   192.168.32.242   node03.suosuoli.cn   <none>           <none>
kube-system   coredns-65c54cc984-26b2k                     1/1     Running   0          38m   172.16.121.129   node01.suosuoli.cn   <none>           <none>
kube-system   coredns-65c54cc984-2ht8v                     1/1     Running   0          38m   172.16.121.131   node01.suosuoli.cn   <none>           <none>
kube-system   etcd-node01.suosuoli.cn                      1/1     Running   0          38m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
kube-system   kube-apiserver-node01.suosuoli.cn            1/1     Running   0          38m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
kube-system   kube-controller-manager-node01.suosuoli.cn   1/1     Running   0          38m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
kube-system   kube-proxy-5xhgk                             1/1     Running   0          20m   192.168.32.242   node03.suosuoli.cn   <none>           <none>
kube-system   kube-proxy-hq6fk                             1/1     Running   0          20m   192.168.32.241   node02.suosuoli.cn   <none>           <none>
kube-system   kube-proxy-k7l27                             1/1     Running   0          38m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
kube-system   kube-scheduler-node01.suosuoli.cn            1/1     Running   0          38m   192.168.32.240   node01.suosuoli.cn   <none>           <none>
root@node01:~# kubectl delete pod kube-proxy-5xhgk -n kube-system
pod "kube-proxy-5xhgk" deleted
root@node01:~# kubectl delete pod kube-proxy-hq6fk -n kube-system
pod "kube-proxy-hq6fk" deleted
root@node01:~# kubectl delete pod kube-proxy-k7l27 -n kube-system
pod "kube-proxy-k7l27" deleted
root@node01:~# kubectl get pods -A | grep kube-proxy
kube-system   kube-proxy-9ltb4                             1/1     Running   0          24s
kube-system   kube-proxy-cghs2                             1/1     Running   0          39s
kube-system   kube-proxy-nk8lx                             1/1     Running   0          14s
root@node01:~# kubectl get pods -A -o wide | grep kube-proxy
kube-system   kube-proxy-9ltb4                             1/1     Running   0          30s   192.168.32.241   node02.suosuoli.cn   <none>           <none>
kube-system   kube-proxy-cghs2                             1/1     Running   0          45s   192.168.32.242   node03.suosuoli.cn   <none>           <none>
kube-system   kube-proxy-nk8lx                             1/1     Running   0          20s   192.168.32.240   node01.suosuoli.cn   <none>           <none>
root@node01:~#

```