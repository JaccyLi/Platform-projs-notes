service account
===

using service account and rolebinding to access cluster apis
---

```bash
root@node01:~/yaml/pod/projected-volume/serviceAccount# kubectl apply -f role-binding-serviceAccount.yaml
role.rbac.authorization.k8s.io/podgetter created
rolebinding.rbac.authorization.k8s.io/podgetter created
serviceaccount/podgetter created

root@node01:~/yaml/pod/projected-volume/serviceAccount# kubectl get role
NAME        CREATED AT
podgetter   2022-02-28T09:36:24Z
root@node01:~/yaml/pod/projected-volume/serviceAccount# kubectl get serviceaccounts
NAME        SECRETS   AGE
default     1         8d
podgetter   1         12s
root@node01:~/yaml/pod/projected-volume/serviceAccount# kubectl get rolebindings.rbac.authorization.k8s.io
NAME        ROLE             AGE
podgetter   Role/podgetter   16s
```

```js
root@test-serviceaccount-pod:/opt/k8s-client# vim javascript/examples/in-cluster.js
root@test-serviceaccount-pod:/opt/k8s-client# cat javascript/examples/in-cluster.js
const k8s = require('@kubernetes/client-node');

const kc = new k8s.KubeConfig();
kc.loadFromCluster();

const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

k8sApi.listNamespacedPod('default')
    .then((res) => {
        console.log(res.body.items[0]);
    })
    .catch((err) => {
        console.log(err);
    });

root@test-serviceaccount-pod:/opt/k8s-client# node javascript/examples/in-cluster.js
V1Pod {
  apiVersion: undefined,
  kind: undefined,
  metadata:
   V1ObjectMeta {
     annotations:
      { 'cni.projectcalico.org/containerID':
         'afb824b474a4a5795325050e04a98aff9b836b48b59e1495ce1fdbcfc37c5ac6',
        'cni.projectcalico.org/podIP': '172.16.120.13/32',
        'cni.projectcalico.org/podIPs': '172.16.120.13/32',
        'kubectl.kubernetes.io/last-applied-configuration':
         '{"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"labels":{"region":"cn-northwest-1"},"name":"test-serviceaccount-pod","namespace":"default"},"spec":{"containers":[{"command":["sleep","3600"],"image":"stevenux/kuberneteslearningimage:latest","name":"test-serviceaccount-pod"}],"serviceAccountName":"podgetter"}}\n' },
     clusterName: undefined,
     creationTimestamp: 2022-02-28T10:01:19.000Z,
     deletionGracePeriodSeconds: undefined,
     deletionTimestamp: undefined,
     finalizers: undefined,
     generateName: undefined,
     generation: undefined,
     labels: { region: 'cn-northwest-1' },
     managedFields:
      [ [V1ManagedFieldsEntry],
        [V1ManagedFieldsEntry],
        [V1ManagedFieldsEntry] ],
     name: 'test-serviceaccount-pod',
     namespace: 'default',
     ownerReferences: undefined,
     resourceVersion: '406488',
     selfLink: undefined,
     uid: 'e650f730-5a3d-4230-a895-bc7b4badf7e1' },
  spec:
   V1PodSpec {
     activeDeadlineSeconds: undefined,
     affinity: undefined,
     automountServiceAccountToken: undefined,
     containers: [ [V1Container] ],
     dnsConfig: undefined,
     dnsPolicy: 'ClusterFirst',
     enableServiceLinks: true,
     ephemeralContainers: undefined,
     hostAliases: undefined,
     hostIPC: undefined,
     hostNetwork: undefined,
     hostPID: undefined,
     hostname: undefined,
     imagePullSecrets: undefined,
     initContainers: undefined,
     nodeName: 'node02.suosuoli.cn',
     nodeSelector: undefined,
     overhead: undefined,
     preemptionPolicy: 'PreemptLowerPriority',
     priority: 0,
     priorityClassName: undefined,
     readinessGates: undefined,
     restartPolicy: 'Always',
     runtimeClassName: undefined,
     schedulerName: 'default-scheduler',
     securityContext:
      V1PodSecurityContext {
        fsGroup: undefined,
        fsGroupChangePolicy: undefined,
        runAsGroup: undefined,
        runAsNonRoot: undefined,
        runAsUser: undefined,
        seLinuxOptions: undefined,
        seccompProfile: undefined,
        supplementalGroups: undefined,
        sysctls: undefined,
        windowsOptions: undefined },
     serviceAccount: 'podgetter',
     serviceAccountName: 'podgetter',
     setHostnameAsFQDN: undefined,
     shareProcessNamespace: undefined,
     subdomain: undefined,
     terminationGracePeriodSeconds: 30,
     tolerations: [ [V1Toleration], [V1Toleration] ],
     topologySpreadConstraints: undefined,
     volumes: [ [V1Volume] ] },
  status:
   V1PodStatus {
     conditions:
      [ [V1PodCondition],
        [V1PodCondition],
        [V1PodCondition],
        [V1PodCondition] ],
     containerStatuses: [ [V1ContainerStatus] ],
     ephemeralContainerStatuses: undefined,
     hostIP: '192.168.32.241',
     initContainerStatuses: undefined,
     message: undefined,
     nominatedNodeName: undefined,
     phase: 'Running',
     podIP: '172.16.120.13',
     podIPs: [ [V1PodIP] ],
     qosClass: 'BestEffort',
     reason: undefined,
     startTime: 2022-02-28T10:01:19.000Z } }
```