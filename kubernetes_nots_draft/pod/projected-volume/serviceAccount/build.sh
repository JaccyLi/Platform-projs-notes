#!/usr/bin/env  bash

docker build -t k8s-client-node .

docker tag k8s-client-node:latest stevenux/kuberneteslearningimage:latest

docker login -u stevenux

docker push stevenux/kuberneteslearningimage:latest