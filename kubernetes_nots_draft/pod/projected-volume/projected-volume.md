 Account 对象的作用，就是 Kubernetes 系统内置的一种“服务账户”，它是 Kubernetes 进行权限分配的对象。
 比如，Service Account A，可以只被允许对 Kubernetes API 进行 GET 操作，而 Service Account B，
 则可以有 Kubernetes API 的所有操作权限。像这样的 Service Account 的授权信息和文件，
 实际上保存在它所绑定的一个特殊的 Secret 对象里的。这个特殊的 Secret 对象，就叫作 ServiceAccountToken。
 任何运行在 Kubernetes 集群上的应用，都必须使用这个 ServiceAccountToken 里保存的授权信息，也就是 Token，
 才可以合法地访问 API Server。所以说，Kubernetes 项目的 Projected Volume 其实只有三种，因为第四种
 ServiceAccountToken，只是一种特殊的 Secret 而已。另外，为了方便使用，Kubernetes 已经为你提供了一个
 默认“服务账户”（default Service Account）。并且，任何一个运行在 Kubernetes 里的 Pod，都可以直接使用
 这个默认的 Service Account，而无需显示地声明挂载它。
 
 这是如何做到的呢？

当然还是靠 Projected Volume 机制。如果你查看一下任意一个运行在 Kubernetes 集群里的 Pod，就会发现，
每一个 Pod，都已经自动声明一个类型是 Secret、名为 default-token-xxxx 的 Volume，然后
自动挂载在每个容器的一个固定目录上。比如

```bash
root@node01:~# kubectl describe pods test-downward-api-pod
Name:         test-downward-api-pod
......
Containers:
  test-downward-api-pod:
......
    Mounts:
      /etc/podinfo from podinfo (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-g7slm (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  podinfo:  # 用户的volume
    Type:         Projected (a volume that contains injected data from multiple sources)
    DownwardAPI:  true
  kube-api-access-g7slm: # 默认volume
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
......

root@node01:~# kubectl describe serviceaccounts
Name:                default
Namespace:           default
Labels:              <none>
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   default-token-bvnn8
Tokens:              default-token-bvnn8
Events:              <none>

root@node01:~# kubectl exec -it test-downward-api-pod -- sh
/ # ls -l /var/run/secrets/kubernetes.io/serviceaccount
total 0
lrwxrwxrwx    1 root     root            13 Feb 25 08:46 ca.crt -> ..data/ca.crt
lrwxrwxrwx    1 root     root            16 Feb 25 08:46 namespace -> ..data/namespace
lrwxrwxrwx    1 root     root            12 Feb 25 08:46 token -> ..data/token
```


